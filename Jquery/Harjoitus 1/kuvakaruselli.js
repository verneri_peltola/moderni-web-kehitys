$(document).ready(function(){
	var nykyinen = 1;
	function vaihdaKuva(kuvaNumero) {
		if(kuvaNumero > 4) {
			kuvaNumero = 1;
		}
		$(".kuva").removeClass("valittu");
		$("#valilehdet span").removeClass("valittu");
		$("#tab"+ kuvaNumero).addClass("valittu");
		$("#kuva" + kuvaNumero).addClass("valittu");
		nykyinen = kuvaNumero;
	}
	var ajastin = setInterval(function(){
		 vaihdaKuva(nykyinen+1);
	}, 5000);
	$("#valilehdet span").click(function(){
		vaihdaKuva($(this).attr("data-kuva-indeksi"));
		clearInterval(ajastin);
		ajastin = setInterval(function(){
			vaihdaKuva(nykyinen+1);
		}, 5000);
	});
	
});