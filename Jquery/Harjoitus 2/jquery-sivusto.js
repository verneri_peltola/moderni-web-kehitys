$(document).ready(function(){
	$("nav h3").click(function(e){
		$(this).parent().children("ul").toggle();
		if($(this).parent().attr("class") == "kiinni") {
			$(this).parent().removeClass("kiinni");
			$(this).parent().addClass("auki");
		} 
		else if($(this).parent().attr("class") == "auki") {
			$(this).parent().removeClass("auki");
			$(this).parent().addClass("kiinni");
		}
		e.stopPropagation();
	});
	$("#sisalto nav ul li").click(function(){
		$("#sisalto nav ul li").removeClass("valittu");
		$(this).addClass("valittu");
		$("#tab1, #tab2, #tab3").removeClass("valittu");
		var indeksi = $(this).attr("data-tab");
		$("#tab" + indeksi).addClass("valittu");
	});
	$("#salasanakentta1").keypress(function(){
		if($(this).val().length >= 8) {
			$(this).css("background-color", "lightgreen");
		}
		else {
			$(this).css("background-color", "pink");
		}
	});
	$("#salasanakentta2").keypress(function(){
		if($(this).val() != $("#salasanakentta1").val()) {
			$(this).css("background-color", "pink");
		}
		else {
			$(this).css("background-color", "lightgreen");
		}
	});
	$("#salasanakentta1").focus(function(){
		$(this).parent().children(".ohje").show();
	});
	$("#salasanakentta1").blur(function(){
		$(this).parent().children(".ohje").hide();
	});
	$("#hyvaksykayttoehdot").change(function(){
		if(!$("#hyvaksykayttoehdot").is(":checked")) {
			$("#rekisteroidy").attr("disabled", true);
		}
		else {
			$("#rekisteroidy").attr("disabled", false);
		}
	});
	$("#rekisteroityminen #rekisteroidy").click(function(){
		$(".rlabel").css("color", "black");
		if($("#nimimerkkikentta").val() == "") {
			$("#nimimerkkikentta").parent().children(".rlabel").css("color", "red");
		}
		if($("#sahkopostikentta").val() == "") {
			$("#sahkopostikentta").parent().children(".rlabel").css("color", "red");
		}
		if($("#salasanakentta1").val() == "") {
			$("#salasanakentta1").parent().children(".rlabel").css("color", "red");
		}
		if($("#salasanakentta2").val() == "") {
			$("#salasanakentta2").parent().children(".rlabel").css("color", "red");
		}
	});
	$("#tab2 #laskentataulukko td").dblclick(function(){
		$("#tab2 #laskentataulukko td").attr("contenteditable", false);
		$(this).attr("contenteditable", true);
	});
	$("#tab2 #laskentataulukko td").keypress(function(e){
		if(e.which == 13) {
			$("#tab2 #laskentataulukko td").attr("contenteditable", false);
		}
	});
	$("#tab2 #lisaarivi").click(function(){
		$("#tab2 #laskentataulukko").append("<tr><td></td><td></td><td></td><td></td><td></td><td class=\"poisto\"><button>Poista</button></td></tr>");
	});
	$("#tab2 #laskentataulukko").on("click", ".poisto", function(){
		$(this).parent().remove();
	});
	$("#tab3 #aloita").click(function(){
		$("#tab3 img").attr("src", "kortti.png");
		var arvauksia = 3;
		function shufle(a) {
			var j, x, i;
			for (i = a.length; i; i -= 1) {
				j = Math.floor(Math.random() * i);
				x = a[i - 1];
				a[i - 1] = a[j];
				a[j] = x;
			}
		}
		var kortit = ["sydan", "sydanrikki", "aurinko", "tyhjakortti", "tyhjakortti", "tyhjakortti", "tyhjakortti", "tyhjakortti", "tyhjakortti", "tyhjakortti", "tyhjakortti", "tyhjakortti"];
		shufle(kortit);
		$("#tab3 img").click(function(){
			$(this).attr("src", kortit[$(this).attr("data-indeksi")-1] + ".png");
			arvauksia--;
			if(kortit[$(this).attr("data-indeksi")-1] == "sydan") {
				$("#viesti").text("Voitit!");
				$("#tab3 img").off("click");
			}
			else if(kortit[$(this).attr("data-indeksi")-1] == "sydanrikki") {
				$("#viesti").text("Hävisit!");
				$("#tab3 img").off("click");
			}
			else if(kortit[$(this).attr("data-indeksi")-1] == "aurinko") {
				arvauksia++;
				$("#viesti").text("arvauksesi jäljellä " + arvauksia);
			}
			else if(kortit[$(this).attr("data-indeksi")-1] == "tyhjakortti") {
				if(arvauksia == 0) {
					$("#viesti").text("Käytit kaikki arvauksesi");
					$("#tab3 img").off("click");
				}
				else {
					$("#viesti").text("arvauksesi jäljellä " + arvauksia);
				}
			}
			$(this).off("click");
		});
	});
});