<!DOCTYPE html>
<?php 
require_once("startsession.php");
require_once("funktiot.php");
?>
<html>
<head>
	<meta charset="UTF-8">
	<title>MuistiApuri</title>
	<link rel="stylesheet" type="text/css" href="muistiapuri.css">
</head>
<body>

<div id="container">
	<header>
		<h1>MuistiApuri - muistilistasi verkossa</h1>
	</header>
	<?php
		if(isset($_SESSION["username"])) {
	?>
	<!-- navi näkyvissä vain kirjautuneena -->
	<nav id="main-navi">
		<ul>
			<li><a href="index.php">Etusivu</a></li>
			<li><a href="notes.php">Muistettavat asiat</a></li>
			<li><a href="note-add.php">Lisää muistettava asia</a></li>
			<li><a href="profile.php">Omat tiedot</a></li>
			<li><a href="manage.php">Hallinnoi kategorioita</a></li>
		</ul>
	</nav>
		<?php } ?> 	
	
	<div id="content">
 