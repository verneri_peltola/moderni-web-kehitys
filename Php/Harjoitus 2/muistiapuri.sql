-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2016 at 07:36 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `muistiapuri`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_swedish_ci NOT NULL,
  `owner` varchar(50) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `owner`) VALUES
(2, 'kotityöt', 'omena'),
(4, 'Puutarhatyöt', 'verneri'),
(5, 'Kauppalista', 'verneri'),
(6, 'Pihatyöt', 'verneri'),
(7, 'Liikunta', 'verneri'),
(8, 'Kotityöt', 'verneri');

-- --------------------------------------------------------

--
-- Table structure for table `note`
--

CREATE TABLE IF NOT EXISTS `note` (
  `note_id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_swedish_ci NOT NULL,
  `category` int(11) NOT NULL,
  `deadline` date NOT NULL,
  `done` tinyint(1) NOT NULL,
  `description` text COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `note`
--

INSERT INTO `note` (`note_id`, `title`, `category`, `deadline`, `done`, `description`) VALUES
(1, 'Suursiivous', 2, '2016-05-20', 1, ''),
(6, 'Astioiden pesu', 2, '2016-05-20', 0, 'astioita'),
(8, 'Kiran ulkoilutus', 2, '2016-05-19', 0, 'vie musti lenkille'),
(10, 'Taloyhtiön kevättalkoot', 4, '2016-05-28', 0, 'Kevättalkoot alkavat klo 8. Siivotaan piha ja kunnstetaan leikkipaikka. Talkoosauna klo 16.'),
(11, 'Kevätistutukset', 4, '2016-06-01', 0, 'Istuta pihanperälle porkkanoita ja sipuleita. Parvekkeelle turvesäkkiin perunoita. Muista käydä ostamassa kukkasipuleita.'),
(12, 'Kesäleiri 27.6. - 3.7.', 7, '2016-06-27', 0, 'Lasten ja nuorten liikuntaleiri Joensuussa. '),
(13, 'Ikkunoiden pesu', 8, '2016-03-24', 1, 'Iskunnat pestävä ennen pääsiäistä! Vieraita tulossa.');

-- --------------------------------------------------------

--
-- Table structure for table `share`
--

CREATE TABLE IF NOT EXISTS `share` (
  `note_id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `share`
--

INSERT INTO `share` (`note_id`, `username`) VALUES
(6, 'verneri'),
(8, 'verneri'),
(12, 'omena');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `nickname` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `email` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `nickname`, `password`, `email`) VALUES
('omena', 'Omena', '47fae73e4472ba72033cb7340420afe7be7b6d8d', 0),
('verneri', 'verneri', '6bb8a6041061ae83df26dc089f4c06afae681c7e', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `owner` (`owner`);

--
-- Indexes for table `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`note_id`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `share`
--
ALTER TABLE `share`
  ADD KEY `note_id` (`note_id`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `note`
--
ALTER TABLE `note`
  MODIFY `note_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `category_fk` FOREIGN KEY (`category`) REFERENCES `category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `share`
--
ALTER TABLE `share`
  ADD CONSTRAINT `note_fk` FOREIGN KEY (`note_id`) REFERENCES `note` (`note_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_fk` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
