<?php require_once("header.php");
	$yhteys = yhdista_tietokantaan();
 ?>
 <?php
	if(isset($_POST["add-category"])) {
		$name = mysql_real_escape_string(strip_tags($_POST["add-category-name"]));
		if($name != "") {
		$sql = "insert into category (name, owner) values ('$name', '" . $_SESSION["username"] . "')";
		
		$tulos = mysql_query($sql, $yhteys);
		}
	}
	if(isset($_GET["deleteid"])) {
		$sql2 = "delete from category where category_id = " . $_GET["deleteid"];
		$tulos2 = mysql_query($sql2, $yhteys);
	}
?>
<main>

<h2>Kategoriat</h2>

<!-- Kategoriat ja kussakin kategoriassa olevien muistettavien asioiden lukumäärä haetaan tietokannasta. -->
<!-- Kategoriat ovat käyttäjäkohtaisia - eli useammalla käyttäjällä voi olla saman niminen, mutta ne ovat silti eri kategorioita. -->

<table id="category-list">
	<tr>
		<th>Kategorian nimi</th>
		<th>Asioita</th>
	</tr>
	<?php
		$sql = "select name, category_id, (select count(*) as maara from note where category = category_id) as maara from category where owner = '" . $_SESSION["username"] . "' ";
		$tulos = mysql_query($sql, $yhteys);
		while($category = mysql_fetch_assoc($tulos)) {
			echo "<tr>";
			echo "<td class=\"category-name\">" . $category["name"] . "</td>";
			echo "<td class=\"notes-in-category\">" . $category["maara"] . "</td>";
			echo "<td><a class=\"delbutton\" href=\"manage.php?deleteid=" . $category["category_id"] . "\" onclick=\"return confirm('Tämä poistaa myös kaikki kategoriassa olevat muistettavat asiat! Oletko varma?');\">Poista</a></td>";
			echo "</tr>";
		}
	?>
	
</table>

<div id="add-category">
	<h3>Lisää uusi kategoria</h3>
	<form action="manage.php" method="post">
		<p><b>Nimi: </b><input type="text" name="add-category-name"></p>
		<p><input type="submit" name="add-category"></p>
	</form>
</div>

</main>
<?php
require_once("sidebar.php");
require_once("footer.php");
?>