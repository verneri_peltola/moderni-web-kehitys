<?php require_once("header.php"); ?>
<main>

<div id="filter-bar">
<?php
$yhteys = yhdista_tietokantaan();
	
?>
	<!-- huolehdi lomaketta käsitellessä, että oikeat valinnat pysyvät valittuina -->
	<form action="notes.php" method="post">
		<select name="note-status">
			<option value="not-done" selected>Tekemättömät</option>
			<option value="done">Tehdyt</option>
			<option value="all">Kaikki</option>
		</select>
		<select name="note-category">
			<option value="all" selected">Kaikki kategoriat</option><!-- joko vain omat (helpompi) tai myös muiden jakamat (vaikeampi) -->
			<?php
				$sql2 = "select * from category where owner = '" . $_SESSION["username"] . "'";
				
				$tulos = mysql_query($sql2, $yhteys);
				while($kategoria = mysql_fetch_assoc($tulos)) {
					echo "<option value=\"" . $kategoria["category_id"] ."\">" . $kategoria["name"] . "</option>";
				}
			?>
			<!-- Hae tähän kategoriat tietokannasta. -->
			<option value="shared" selected">Muiden jakamat</option>
		</select>
		<input type="submit" name="filter-submit" value="Hae">
	</form>
</div>
<?php
	$sql = "";
	$status = null;
	$category = null;
	if(isset($_POST["filter-submit"])) {
		$status = mysql_real_escape_string($_POST["note-status"]);
		$category = mysql_real_escape_string($_POST["note-category"]);
		
		$sql = "select * from note, category where note.category = category.category_id and category.owner = '" . $_SESSION["username"] . "' ";
		if($category != "all" && $category != "shared") {
			$sql .= "and category = $category";
			if($status == "done") {
				$sql .= " and done = 1";
			}
			elseif($status == "not-done") {
				$sql .= " and done = 0";
			}
		}
		elseif($category == "all") {
			if($status == "done") {
				$sql .= " and done = 1";
			}
			elseif($status == "not-done") {
				$sql .= " and done = 0";
			}
		}
		else {
			$sql = "select * from share, note, category, user where share.username = '" . $_SESSION["username"] . "' and share.note_id = note.note_id and note.category = category.category_id and user.username = category.owner";
		}
	}
	else {
		$sql = "select * from note, category where note.category = category.category_id and category.owner = '" . $_SESSION["username"] . "' and done = 0";
	}
		$tulos2 = mysql_query($sql, $yhteys);
		if(!$tulos2) {
			die(mysql_error());
		}
		while($note = mysql_fetch_assoc($tulos2)) {
			echo "<div class=\"note\">";
			if($category == "shared") {
				echo "<span class=\"shared-by\">" . $note["nickname"] . "</span>"; 
			}
			echo "<h3><a href=\"note-view.php?noteid=" . $note["note_id"] . "\">" . $note["title"] . "</a></h3>";
			echo "<p><b>Kategoria: </b>" . $note["name"] . " || <b>Aikaraja: </b> " . $note["deadline"] . "<p>";
			echo "<p>" . $note["description"] . "</p>";
			echo "</div>";
		}
	
?>

</main>
<?php
require_once("sidebar.php");
require_once("footer.php");
?>