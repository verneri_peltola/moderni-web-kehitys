<?php require_once("header.php");
	$yhteys = yhdista_tietokantaan();
 ?>
<main>
<h2>Lisää muistettava asia</h2>
<?php
	if(isset($_POST["add-note"])) {
		$name = mysql_real_escape_string(strip_tags($_POST["note-title"]));
		$description = mysql_real_escape_string(strip_tags($_POST["note-desc"]));
		$category = mysql_real_escape_string(strip_tags($_POST["note-category"]));
		$deadline = mysql_real_escape_string(strip_tags($_POST["note-deadline"]));
		if($name != "" && $description != "" && $category != "" && $deadline != null) {
			$sql2 = "insert into note (title, category, description, deadline) values ('$name',$category, '$description', '$deadline')";
			$tulos2 = mysql_query($sql2, $yhteys);
			if(!$tulos2) {
				exit("Tietokantaoperaatio epäonnistui: " . mysql_error());
			}
		}
	}
	
?>
<form action="note-add.php" method="post">
	<p><span class="note-add-label"><b>Nimi: </b></span><input type="text" name="note-title"></p>

	<p><b>Kuvaus</b><br>
	<textarea name="note-desc" rows="5" cols="50"></textarea>
	</p>
	<!-- hae kategoriat tietokannasta -->
	<?php
		$sql = "select name, category_id, (select count(*) as maara from note where category = category_id) as maara from category where owner = '" . $_SESSION["username"] . "' ";
		$tulos = mysql_query($sql, $yhteys);
		
	?>
	<p><span class="note-add-label"><b>Kategoria: </b></span>
		<select name="note-category">
			<option value="">Valitse kategoria...</option>
			<?php 
				while($kategoria = mysql_fetch_assoc($tulos)) {
					echo "<option value=\"" . $kategoria["category_id"] ."\">" . $kategoria["name"] . "</option>";
				}
			?>
		</select>
	</p>
	<p><span class="note-add-label"><b>Aikaraja: </b></span><input type="date" name="note-deadline"></p>
	<p><input type="submit" name="add-note" value="Tallenna"></p>
</form>

</main>
<?php
require_once("sidebar.php");
require_once("footer.php");
?>