<?php require_once("header.php"); ?>
<main>

<h2>Rekisteröidy</h2>
<?php

	$yhteys = yhdista_tietokantaan();
	
	$ronnistui = false;
	
	$tunnustyhja = false;
	$nimityhja = false;
	$salasanatyhja = false;
	$spostityhja = false;
	$salasanatEisamat = false;
	
	if(isset($_POST["register-submit"])) {
		$username = mysql_real_escape_string(strip_tags($_POST["username"]));
		$nickname = mysql_real_escape_string(strip_tags($_POST["nickname"]));
		$password1 = mysql_real_escape_string(strip_tags($_POST["password1"]));
		$password2 = mysql_real_escape_string(strip_tags($_POST["password2"]));
		$email = mysql_real_escape_string(strip_tags($_POST["email"]));
		
		if($username != "" && $nickname != "" && $password1 != "" && $password2 == $password1 && $email != "") {
			$sql2 = "select * from user where username = '$username'";
			$hakutulos = mysql_query($sql2, $yhteys);
			if(!$hakutulos) {
				exit("Tietokantahaku epäonnistui: " . mysql_error());
			}
			
			if(mysql_fetch_assoc($hakutulos)) {
				echo "Käyttäjätunnus käytössä";
			}
			else {
				$sql = "insert into user (username, nickname, password, email) values ('$username', '$nickname', '" . sha1($password1) . "', '$email')";
				$tulos = mysql_query($sql, $yhteys);
				if($tulos) {
					$ronnistui = true;
					?>
					<p>Rekisteröityminen onnistui. Kirjaudu sisään.</p>
					<div id="login-box-content">
			<!-- vain ei-kirjautuneena -->
			<!-- login.php näyttää virheilmoituksen jos tunnus / salasana väärin, muuten kirjaa käyttäjän sisään ja ohjaa etusivulle -->
			<form action="login.php" method="post">
				<p><span class="login-label">Tunnus: </span><input type="text" name="username"></p>
				<p><span class="login-label">Salasana: </span><input type="password" name="password"></p>
				<p><input type="submit" name="login-submit" value="Kirjaudu"></p>
			</form>
		</div>
		<?php
				}
				else {
					echo "rekisteröityminen epäonnistui";
				}
			}
		}
		else {
			if ($username == "") {
				$tunnustyhja = true;
			}
			if ($nickname == "") {
				$nimityhja = true;
			}
			if ($email == "") {
				$spostityhja = true;
			}
			if ($password1 == "") {
				$salasanatyhja = true;
			}
			elseif ($password1 != $password2) {
				$salasanatEisamat = true;
			}
			
		}
	}
	if(!$ronnistui) {
?>
<form action="register.php" method="post">
	<!-- Käyttäjätunnus ei saa olla jo tietokannassa esiintyvä. Kaikki kentät pitää täyttää. -->
	<!-- Mahdolliset virheilmoitukset tulee näyttää lomakkeen yhteydessä. Jos rekisteröityminen onnistuu, lomaketta ei näytetä,
	vaan ilmoitus "Rekisteröityminen onnistui. Kirjaudu sisään." ja samanlainen kirjautumislaatikko kuin sivupalkissa. -->
	<p><span class="register-label">Käyttäjätunnus: </span><input type="text" name="username"><?php if($tunnustyhja){echo "Täytä kenttä";} ?></p>
	<p><span class="register-label">Nimi: </span><input type="text" name="nickname"><?php if($nimityhja){echo "Täytä kenttä";} ?></p>
	<p><span class="register-label">Salasana: </span><input type="password" name="password1"><?php if($salasanatyhja){echo "Täytä kenttä";} ?></p>
	<p><span class="register-label">Salasana uudelleen: </span><input type="password" name="password2"><?php if($salasanatyhja){echo "Täytä kenttä";}if($salasanatEisamat){echo "Salasanat eivät täsmää";} ?></p>
	<p><span class="register-label">Sähköposti: </span><input type="email" name="email"><?php if($spostityhja){echo "Täytä kenttä";} ?></p>
	<p><input type="submit" name="register-submit" value="Rekisteröidy"></p>
</form>
	<?php } ?>
</main>
<?php
require_once("sidebar.php");
require_once("footer.php");
?>