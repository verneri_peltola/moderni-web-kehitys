<?php
require_once("funktiot.php");
 if(!onKatseluOikeus($_GET["noteid"])) {
	header("Location: index.php");
	die();
}
?>
<?php require_once("header.php"); ?>
<main>
<?php
	$yhteys = yhdista_tietokantaan();
	$oma = true;
	$omistaja = null;
	$sql = "select * from note, category where note_id = " . $_GET["noteid"] . " and note.category = category.category_id";
	$tulos = mysql_query($sql, $yhteys);
	if(!$tulos) {
		die(mysql_error());
	}
	if($note = mysql_fetch_assoc($tulos)) {
		if($note["owner"] == $_SESSION["username"]) {
			$oma = true;
		}
		else {
			$oma = false;
			$omistaja = $note["owner"];
		}
?>
<!-- poimi GET-parametri ja hae tietokannasta vastaava muistettava asia -->
<!-- TARKISTA ETTÄ KÄYTTÄJÄ OMISTAA SEN TAI SE ON JAETTU HÄNELLE -->
<h2><?php echo $note["title"]; if($note["done"] == 1) { echo '<span class="note-done">Tehty</span>'; }?></h2>

<p><b>Kategoria: </b><?php echo $note["name"]; ?> || <b>Aikaraja: </b><?php echo $note["deadline"]; ?></p>

	<p><?php echo $note["description"]; }?></p>
<?php
if($oma) {
	$sql2 = "select * from share, user where share.username = user.username and note_id = " . $_GET["noteid"] . "";
	$tulos2 = mysql_query($sql2, $yhteys);
	if(!$tulos2) {
		die(mysql_error());
	}
	if(mysql_num_rows($tulos2) > 0) {
		echo "<p><b>Jaettu</b></p>";
	}
	echo "<ul>";
	while($jaettu = mysql_fetch_assoc($tulos2)) {
		echo "<li>" . $jaettu["nickname"] . "<a class=\"remove-share\" href=\"note-modify.php?removeshare=1&user=1\">Poista jakaminen</a></li>";
	}
	echo "</ul>";
}
else {
	echo "<p>Tämän muistettavan asian sinulle jakoi: <b>" . $omistaja . "</b></p>";
}
if($oma) {
?>
<!-- vain omistajalle -->
<div id="note-modify-buttons">
	<a href="note-modify.php?modifynote=<?php echo $_GET["noteid"]; ?>">Muokkaa</a>
	<a href="note-modify.php?deletenote=<?php echo $_GET["noteid"]; ?>">Poista</a>
</div>
<?php } ?>

</main>
<?php
require_once("sidebar.php");
require_once("footer.php");
?>