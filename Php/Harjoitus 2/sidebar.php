<div id="sidebar">
	<div id="login-box">
		<?php 
		$yhteys = yhdista_tietokantaan();
			if(!isset($_SESSION["username"])) {
		?>
		<h2>Kirjaudu sisään</h2>
			<?php } ?>
		<div id="login-box-content">
		<?php
			if(!isset($_SESSION["username"])) {
		?>
			<!-- vain ei-kirjautuneena -->
			<!-- login.php näyttää virheilmoituksen jos tunnus / salasana väärin, muuten kirjaa käyttäjän sisään ja ohjaa etusivulle -->
			<form action="login.php" method="post">
				<p><span class="login-label">Tunnus: </span><input type="text" name="username"></p>
				<p><span class="login-label">Salasana: </span><input type="password" name="password"></p>
				<p><input type="submit" name="login-submit" value="Kirjaudu"></p>
			</form>
			<p class="footnote">Ei tunnuksia? <a href="register.php">Rekisteröidy!</a>
			<?php 
			} 
			else {
			?>
			<!-- vain kirjautuneena -->
			<p>Olet kirjautunut: <b><?php echo $_SESSION["username"]; ?></b></p>
			<p><a href="login.php?logout=true">Kirjaudu ulos</a></p>
			<?php } ?>
		</div>
	</div>
	<?php 
		if(isset($_SESSION["username"])) {
	?>
	<!-- vain kirjautuneena -->
	<div id="urgent-notes">
		<h2>Kiireelliset asiat</h2>
		<div id="urgent-notes-content">
		<?php
			$sql = "select * from note, category where note.category = category.category_id and category.owner = '" . $_SESSION["username"] . "' and done = 0 order by deadline";
			$tulos = mysql_query($sql, $yhteys);
			if(!$tulos) {
				die(mysql_error());
			}
			if(mysql_num_rows($tulos) == 0){
				echo "Ei kiirellisiä asioita";
			}
			while($asiat = mysql_fetch_assoc($tulos)) {
				echo "<div class=\"urgent-note\">";
				echo "<span class=\"deadline\">" . $asiat["deadline"] . "</span>";
				echo "<span class=\"topic\"><b><a href=\"note-view.php?noteid=" . $asiat["note_id"] . "\">" . $asiat["title"] . "</a></b></span>";
				echo "</div>";
			}
		?>
		</div>
	</div>
	<?php } ?>
</div>