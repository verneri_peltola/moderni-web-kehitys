<?php
require_once("funktiot.php");
 if(!onMuokkausOikeus($_GET["modifynote"])) {
	header("Location: index.php");
	die();
}
?>
<?php require_once("header.php"); ?>
<main>
<h2>Muokkaa muistettavaa asiaa</h2>
<?php
	$yhteys = yhdista_tietokantaan();
	
	if(isset($_POST["modify-note"])) {
		$name = mysql_real_escape_string(strip_tags($_POST["note-title"]));
		$description = mysql_real_escape_string(strip_tags($_POST["note-desc"]));
		$category = mysql_real_escape_string($_POST["note-category"]);
		$deadline = mysql_real_escape_string(strip_tags($_POST["note-deadline"]));
		$done = mysql_real_escape_string(strip_tags($_POST["note-done"]));
		$note = mysql_real_escape_string($_GET["modifynote"]);
		if(isset($done) && $done == "done") {
			$done = 1;
		}
		else {
			$done = 0;
		}
		$sql = "update note set title = '$name', description = '$description', category = $category, deadline = '$deadline', done = $done where note_id = $note";
		$tulos = mysql_query($sql, $yhteys);
		if(!$tulos) {
			exit("Tietokantaoperaatio epäonnistui: " . mysql_error());
		}
	}
	if(isset($_GET["modifynote"])) {
		$note = mysql_real_escape_string($_GET["modifynote"]);
		$sql2 = "select * from note where note_id = $note";
		$tulos2 = mysql_query($sql2, $yhteys);
		if(!$tulos2) {
			die(mysql_error());
		}
		while($tiedot = mysql_fetch_assoc($tulos2)) {
			
	
?>
<!-- noteid siirrettävä GET-parametrista uuteen linkkiin -->
<form action="note-modify.php?modifynote=<?php echo $note; ?>" method="post">
	<p><span class="note-modify-label"><b>Nimi: </b></span><input type="text" name="note-title" value="<?php echo $tiedot["title"]; ?>"></p>

	<p><b>Kuvaus</b><br>
	<textarea name="note-desc" rows="5" cols="50"><?php echo $tiedot["description"]; ?></textarea>
	</p>
	<!-- hae kategoriat tietokannasta -->
	<p><span class="note-modify-label"><b>Kategoria: </b></span>
		<select name="note-category">
			<option value="">Valitse kategoria...</option>
			<?php
				$sql3 = "select * from category where owner = '" . $_SESSION["username"] . "'";
				$tulos3 = mysql_query($sql3, $yhteys);
				while ($kategoria = mysql_fetch_assoc($tulos3)) {
					if($tiedot["category"] == $kategoria["category_id"]) {
						echo "<option value=\"" . $kategoria["category_id"] . "\" selected>" . $kategoria["name"] . "</option>";
					}
					else {
						echo "<option value=\"" . $kategoria["category_id"] . "\">" . $kategoria["name"] . "</option>";
					}
				}
			?>
		</select>
	</p>
	<p><span class="note-modify-label"><b>Aikaraja: </b></span><input type="date" name="note-deadline" value="<?php echo $tiedot["deadline"]; ?>"></p>
	<p><span class="note-modify-label"><b>Tehty: </b></span><input type="checkbox" value="done" name="note-done"<?php if($tiedot["done"] == 1) { echo " checked";} ?>></p>
	<p><input type="submit" name="modify-note" value="Tallenna"></p>
</form>
<?php
	}
	}
?>
<h3>Jaa muistettava asia</h3>
<?php
	if(isset($_POST["share-note"])) {
		$share = mysql_real_escape_string($_POST["share-with"]);
		$noteid = mysql_real_escape_string($_GET["modifynote"]);
		if($share != "") {
			$sql2 = "insert into share (note_id, username) values ($noteid, '$share')";
			$tulos2 = mysql_query($sql2, $yhteys);
			if($tulos2) {
				echo "Jakaminen onnistui";
			}
			else {
				echo "Jakaminen epäonnistui" . mysql_error();
			}
		}
	}
	
?>
<form action="note-modify.php?modifynote=<?php echo $_GET["modifynote"]; ?>" method="post">
	<p>
		
		<!-- hae käyttäjänimet tietokannasta -->
		<select name="share-with">
			<option value="">Valitse käyttäjä...</option>
			<?php
				$sql4 = "select * from user";
				$tulos4 = mysql_query($sql4, $yhteys);
				while($users = mysql_fetch_assoc($tulos4)) {
					echo "<option value=\"" . $users["username"] . "\">" . $users["username"] . "</option>";
				}
				
			?>
		</select>

	<input type="submit" name="share-note" value="Jaa">
	</p>
</form>

</main>
<?php
require_once("sidebar.php");
require_once("footer.php");
?>