<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="asemmointi.css">
		<title>Etusivu</title>
	</head>
	<body>
		<div id="container">
			<header>
				<h1>Satuvaltakunnan tarinat</h1>
				<h3>Uutisia lumotusta maasta</h3>
			</header>
			<div id="saa">
				<h3 id="otsikko">Viikon sää - viikko 21</h3>
				<?php
				require("funktiot.php");
				$yhteys = yhdista_tietokantaan();
				
					$xml=simplexml_load_file("http://localhost/porfoilio1/xml.php") or die("Virhe: XML-syötteen käsittely epäonnistui");
					foreach($xml->children() as $saa) {
						echo "<div class=\"box\">";
						$date = date_create($saa["pvm"]);
						echo "<b>" . date_format($date, "d.m.Y") . "</b>";
						echo "<h3>" . $saa->lampotila . " &deg;C</h3>";
						echo "<p><b>Säätila:</b></p>" . utf8_decode($saa->saatila) . "<br><br>";
						echo "<p><b>Tuulennopeus:</b></p>" . $saa->tuulennopeus . " m/s<br><br>";
						echo "</div>";
					}
				?>
			</div>
			<div id="uutiset">
				<?php
					$sql2 = "select * from uutiset where paauutinen = 1 order by julkaisuaika desc limit 2 ";
					$tulos2 = mysql_query($sql2, $yhteys);
					if(!$tulos2) {
						exit("Tietokantaoperaatio epäonnistui: " . mysql_error());
					}
					while($uutinen = mysql_fetch_assoc($tulos2)) {
						echo "<div class=\"uutinen\">";
						echo "<h3 class=\"otsikko2\">" . $uutinen["otsikko"] . "</h3>";
						$date2 = date_create($uutinen["julkaisuaika"]);
						echo "<p class=\"aika\">" . date_format($date2, "d.m.Y") . " klo " . date_format($date2, "H:i") . " || " . $uutinen["kirjoittaja"] . "</p>";
						echo "<p class=\"teksti\">" . $uutinen["sisalto"] . "</p>";
						echo "</div>";
					}
				?>
			</div>
			<div id="sivupalkki">
				<div id="uusimmat">
					<h3>Uusimmat uutiset</h3>
					<?php
						$sql = "select * from uutiset order by julkaisuaika desc";
						$tulos = mysql_query($sql, $yhteys);
						while($uusimmat = mysql_fetch_assoc($tulos)) {
							echo "<div class=\"border\">";
							echo "<p class=\"otsikko4\"><b>" . $uusimmat["otsikko"] . "</p></b>";
							$date3 = date_create($uusimmat["julkaisuaika"]);
							echo "<p class=\"aika\">" . date_format($date3, "d.m.Y") . " klo " . date_format($date3, "H:i") . "</p>";
							echo "</div>";
						}
					?>
				</div>
				<div id="blogit">
					<h3>Vierailevat kirjoittajat</h3>
					<?php
						$str = file_get_contents('http://localhost/porfoilio1/json.php');
				
						$json = json_decode($str, true);
						
						foreach($json as $blogi) {
							echo "<div class=\"border\">";
							echo "<p class=\"otsikko3\"><b>" . utf8_decode($blogi["nimi"]) . "</b>: " . utf8_decode($blogi["otsikko"]);
							$date4 = date_create($blogi["julkaisuaika"]);
							echo "<p class=\"aika2\">" . date_format($date4, "d.m.Y") . "</p>";
							echo "<p class=\"kirjoittaja\">". utf8_decode($blogi["kirjoittaja"]) . "</p>";
							echo "</div>";
						}
					?>
				</div>
			</div>
		</div>
	</body>
</html>