<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="asemmointi.css">
		<title>Kysymykset</title>
	</head>
	<body>
		<div id="container">
			<header>
				<h1><a href="etusivu.php">Suuri kysymyspalsta</a></h1>
			</header>
			<div id="kategorianNimi">
				<?php
					require("funktiot.php");

					$yhteys = yhdista_tietokantaan();
			
					session_start();
				
					if(isset($_GET["kategoria"])) {
						// koodaa parametri turvalliseksi tietokantaa varten - tämä estää sekä tahattomia ongelmia syötteiden kanssa että hakkerointia
						$kategoria = mysql_real_escape_string($_GET["kategoria"]);

						// muodostetaan SQL-kysely -- MUOKKAA TÄTÄ
						$sql = "select kategoria.kategoria as kategoria from kysymys, kategoria where kysymys.kategoria='$kategoria' and kysymys.kategoria = kategoria.kategoria_id";
						  
						// noudetaan kyselyn tulos muuttujaan - tässä muuttujassa ei siis ole vielä itse tietueita
						$tulos = mysql_query($sql, $yhteys);
						if(!$tulos) {
							echo mysql_error();
						}
						if($kategoria = mysql_fetch_assoc($tulos)) {
							echo "<h2>" . $kategoria["kategoria"] . "</h2>";
						}
					}
				?>
			</div>
			<?php
				
				
				if(isset($_GET["kategoria"])) {
				  // koodaa parametri turvalliseksi tietokantaa varten - tämä estää sekä tahattomia ongelmia syötteiden kanssa että hakkerointia
				  $kategoria = mysql_real_escape_string($_GET["kategoria"]);

				  // muodostetaan SQL-kysely -- MUOKKAA TÄTÄ
				  $sql = "select * from kysymys where kategoria='$kategoria' order by paivamaara DESC";
				  
				  // noudetaan kyselyn tulos muuttujaan - tässä muuttujassa ei siis ole vielä itse tietueita
				  $tulos = mysql_query($sql, $yhteys);
				  
				  // tarkistetaan, onnistuiko kysely
				  if(!$tulos)
					exit("Tietokantahaku epäonnistui: " . mysql_error());

				  // noudetaan kyselyn seuraava rivi muuttujaan $auto ja jatketaan niin kauan kuin rivejä on
				  while($kategoria = mysql_fetch_assoc($tulos)) {
					echo "<div id=\"kysymykset\">";
					
					echo "<div class=\"kysymyksia\"><a href=\"kysymys.php?kysymys=" . $kategoria["id"] . "\">";
					
					echo "<p class=\"kysymys\">" . $kategoria["otsikko"] . "</p>";
					echo "<p class=\"date\">" . $kategoria["paivamaara"] . "</p>";
					echo "<p class=\"kysyja\">" . $kategoria["kysyjan_nimimerkki"] . "</p>";
					
					echo "</a></div>";
					echo "</div>";
				  }
				}
			?>
		</div>
	</body>
</html>