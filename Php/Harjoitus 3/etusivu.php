<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="asemmointi.css">
		<title>Etusivu</title>
	</head>
	<body>
		<?php
			require("funktiot.php");

			$yhteys = yhdista_tietokantaan();
			$lisaysTyhja = false;
			session_start();
					if(isset($_POST["lisaa"])) {
						// siirretään lomakkeen tiedot muuttujiin
						$otsikko = mysql_real_escape_string($_POST["otsikko"]);
						$sisalto = mysql_real_escape_string($_POST["sisalto"]);
						$kategoria= mysql_real_escape_string($_POST["kategoria"]);
						if($sisalto == "" || $otsikko == "") {
							$lisaysTyhja = true;
						}
						else {
						// muodostetaan SQL-kysely -- MUOKKAA TÄTÄ
						$sql = "insert into kysymys (otsikko, sisalto, paivamaara, kirjoittaja, kategoria, kysyjan_nimimerkki) values ('$otsikko', '$sisalto', now(), " . $_SESSION["kid"] . ", $kategoria, '". $_SESSION["ktunnus"]. "')";
						// suoritetaan kysely -- tällä kertaa ei haeta mitään, joten muuttuja $tulos on vain kyselyn onnistumisen tarkistamista varten
						$tulos = mysql_query($sql, $yhteys);
echo $sql;
						// jos lisäys onnistui, muuttujassa $tulos on arvo true; jos ei, niin false
						if($tulos) {
							echo "<p><b>Kysymys on lisätty</b></p>";
						}
						else {
							exit("Tietokantaoperaatio epäonnistui: " . mysql_error());
						}
					}
					}
		?>
		<div id="container">
			<header>
				<h1><a href="etusivu.php">Suuri kysymyspalsta</a></h1>
			</header>
			<div id="kategoriat">
				<div class="kategoria">
					<p><a href="kysymykset.php?kategoria=1">Pelit >></a></p>
				</div>
				<div class="kategoria">
					<p><a href="kysymykset.php?kategoria=5">Terveys >></a></p>
				</div>
			</div>
			<?php
				
				if(isset($_GET["ulos"])) {
					session_unset();
					session_destroy();
				}
				if(!isset($_SESSION["sposti"])) {
					if(isset($_POST["kirjaudu"])) {
					
					$sposti = mysql_real_escape_string($_POST["sposti"]);
					$salasana = mysql_real_escape_string($_POST["ssana"]);
					
					$sql = "select * from kayttaja where sposti = '$sposti' && salasana = '$salasana'";
					
					$tulos = mysql_query($sql, $yhteys);
					if(!$tulos) {
						exit("Tietokantahaku epäonnistui: " . mysql_error());
					}
					if($kayttaja = mysql_fetch_assoc($tulos)) {
						$_SESSION["sposti"] = $kayttaja["sposti"];
						$_SESSION["ktunnus"] = $kayttaja["nimimerkki"];
						$_SESSION["kid"] = $kayttaja["id"];
					}
					else {
						echo "Käyttäjätunnus tai salasana väärin";
					}
					}
				}
				if(!isset($_SESSION["sposti"])) {
			?>
			<div id="sivupalkki">
				<div id="login">
					<h3>Kirjaudu</h3>
					<form action="etusivu.php" method="post">
						<div class="label">Sähköposti:</div> <input type="email" name="sposti"><br>
						<div class="label">Salasana:</div> <input type="password" name="ssana"><br>
						<input type="submit" value="kirjaudu" name="kirjaudu">
					</form>
				</div>
				<?php
				if(isset($_POST["rekisteroidy"])) {
					// siirretään lomakkeen tiedot muuttujiin
					$nimi = mysql_real_escape_string($_POST["nimi"]);
					$sposti= mysql_real_escape_string($_POST["sposti2"]);
					$salasana= mysql_real_escape_string($_POST["salasana"]);
					
					// muodostetaan SQL-kysely -- MUOKKAA TÄTÄ
					$sql = "insert into kayttaja (nimimerkki, sposti, salasana) values ('$nimi', '$sposti', '$salasana')";

					// suoritetaan kysely -- tällä kertaa ei haeta mitään, joten muuttuja $tulos on vain kyselyn onnistumisen tarkistamista varten
					$tulos = mysql_query($sql, $yhteys);
					if($tulos) {
						echo "<p id=\"ilmoitus\">Käyttäjä on lisätty</p>";
					}
				}
				?>
				<div id="rekisteroidy">
					<h3>Rekisteröidy</h3>
					<form action="etusivu.php" method="post">
						<div class="label">Nimimerkki:</div> <input type="text" name="nimi"><br>
						<div class="label">Sähköposti:</div> <input type="email" name="sposti2"><br>
						<div class="label">Salasana:</div> <input type="password" name="salasana"><br>
						<input type="submit" value="rekisteroidy" name="rekisteroidy">
					</form>
				</div>
			</div>
			<?php
				}
				else {
			?>
			<div id="ylapalkki">
				<p><a href="etusivu.php?ulos=k">Kirjudu ulos</a></p>
			</div>
			<div id="uusiKysymys" >
				<h2>Lisää kysymys</h2>
				<form action="etusivu.php" method="post">
					<p>Otsikko: <input type="text" name="otsikko" value="<?php 
						if(isset($_POST["otsikko"])) {
							echo $_POST["otsikko"];
						}
					?>"></p>
					Kuvaus: <?php
				if($lisaysTyhja) {
					echo "<span style=\"color: red\">Täytä molemmat kentät</span>";
				}
				?><br>
					<textarea cols="50" rows="4" name="sisalto"><?php 
						if(isset($_POST["sisalto"])) {
							echo $_POST["sisalto"];
						}
					?></textarea><br>
					<select name="kategoria">
						<option value="1">Pelit</option>
						<option value="5">Terveys</option>
					</select><br><br>
					<input type="submit" name="lisaa">
				</form>
				<?php } ?>
			</div>
		</div>
	</body>
</html>