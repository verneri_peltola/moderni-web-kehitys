Neuvontapalsta

Vierailia

Rekisteröityminen
Vierailijat voivat rekisteröityä järjestelmään syöttämällä nimimerkin, salasanan ja sähköpostiosoiteen.

Kirjautuminen
Vierailijat voivat kirjautua järjestelmään käyttäen sähköpostiosoitetta ja salasanaa.

Kysymysten selaaminen
Vierailijat voivat selata kysymiksiä ja niiden vastauksia. Sivulla näytetään kysymys ja kysyjän nimimerkki.

Käyttäjä

Kysyminen
Rekisteröityneet käyttäjät voivat kirjoittaa minkätansa kysymyksen. Jokaiseen kysymykseen kuuluu otsikko, sisältö, päivämäärä ja kysyjän nimimerkki. Kysymykset on jaiteltu ketegorioihin.

Vastaaminen
Rekisteröityneet käyttäjät voivat vastata mihin tahansa kysymykseen. Jokiseen vastaukseen kuuluu sisältö ja vastaajan nimimerkki.

Kysysmysten selaaminen
Käyttäjät voivat selata kysymyksiä ja niiden vastauksia. Sivulla näytetään kysymys ja kysyjän nimimerkki.