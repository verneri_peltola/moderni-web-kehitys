-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2016 at 11:57 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Chat-huone`
--

-- --------------------------------------------------------

--
-- Table structure for table `viestit`
--

CREATE TABLE IF NOT EXISTS `viestit` (
  `nimimerkki` varchar(30) COLLATE utf8mb4_swedish_ci NOT NULL,
  `aika` datetime NOT NULL,
  `viesti` varchar(200) COLLATE utf8mb4_swedish_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_swedish_ci;

--
-- Dumping data for table `viestit`
--

INSERT INTO `viestit` (`nimimerkki`, `aika`, `viesti`, `id`) VALUES
('kissa', '2016-06-02 11:43:18', '', 4),
('moi', '2016-06-02 11:48:11', '', 5),
('kissa', '2016-06-02 11:50:19', '', 6),
('kissa', '2016-06-02 11:51:30', '', 7),
('kissa', '2016-06-02 11:52:29', '', 8),
('kissa', '2016-06-02 11:53:02', 'moi', 9),
('kissa', '2016-06-02 11:56:10', 'moi', 10),
('moi', '2016-06-02 12:10:27', '', 11),
('kissa', '2016-06-02 12:11:41', '', 12),
('kissa', '2016-06-02 12:12:30', '', 13),
('kissa', '2016-06-02 12:17:21', '', 14),
('kissa', '2016-06-02 12:17:53', '', 15),
('kissa', '2016-06-02 12:21:32', '', 16),
('kissa', '2016-06-02 12:40:11', '', 17),
('kissa', '2016-06-02 12:40:53', '', 18),
('kissa', '2016-06-02 12:42:02', '', 19),
('kissa', '2016-06-02 12:44:32', '', 20),
('kissa', '2016-06-02 12:45:03', '', 21),
('kissa', '2016-06-02 12:46:32', '', 22),
('kissa', '2016-06-02 12:46:54', 'kissa', 23),
('moi', '2016-06-02 12:47:17', 'kissa', 24),
('moi', '2016-06-02 12:47:56', 'kisa', 25),
('kissa', '2016-06-02 12:48:07', 'sia', 26),
('kisaa', '2016-06-02 12:48:20', 'kissa', 27),
('kissa', '2016-06-02 12:54:11', 'koira', 28),
('kissa', '2016-06-02 12:56:03', 'koiria', 29),
('moi', '2016-06-02 12:56:24', 'Terve', 30);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `viestit`
--
ALTER TABLE `viestit`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `viestit`
--
ALTER TABLE `viestit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
