$(document).ready(function(){
	$.get("keskustelu.php", function(keskustelu){
		$("#keskustelu").html(keskustelu);
	});
	$("#laheta").click(function(){
		$.post("viestintallennus.php", {nimimerkki: $("#nimimerkki").val(), viesti: $("#viesti").val()}, function(viesti){
			$.get("keskustelu.php", function(keskustelu){
				$("#keskustelu").html(keskustelu);
			});
			$("#viesti").val("");
		});
		
	});
	setInterval(function(){
		$.get("keskustelu.php", function(keskustelu){
			$("#keskustelu").html(keskustelu);
		});
	}, 5000);
});