$(document).ready(function(){
	var asiakirja_id = "kaikki";
	vaihdaAsiakirjaa();
	$("#asiakirjavalinta").change(function(){
		asiakirja_id = $("#asiakirjavalinta").val();
		vaihdaAsiakirjaa();
	});
	$("#laheta-kommentti").click(function(){
		if($("#kommenttinimimerkki").val() != "" && $("#kommenttiotsikko").val() != "" && $("#kommenttiteksti").val() != "") {
			$.post("kayttaja.php", {kommenttinimimerkki: $("#kommenttinimimerkki").val(), kommenttiotsikko: $("#kommenttiotsikko").val(), kommenttiteksti: $("#kommenttiteksti").val(), asiakirja_id: asiakirja_id}, function(data){
				$("#virheilmoitus").text("");
				$.get("kommentit.php", {id: asiakirja_id}, function(kommentit){
					$("#kommenttisailio").html(kommentit);
				});
				$("#kommenttinimimerkki").val("");
				$("#kommenttiotsikko").val("");
				$("#kommenttiteksti").val("");
			});
			
		}
		else {
			$("#virheilmoitus").text("Täytä kaikki kentät");
		}
		
	});
	$("#artikkeliSailio").on("click", "article" ,function(){
		if($(this).attr("data-asiakirja-numero")) {
			asiakirja_id = $(this).attr("data-asiakirja-numero");
			$("#asiakirjavalinta").val(asiakirja_id);
			vaihdaAsiakirjaa();
		}
	});
	function vaihdaAsiakirjaa() {
		if(asiakirja_id != "kaikki") {
			$.get("asiakirja.php", {id: asiakirja_id}, function(asiakirja){
				$("#artikkeliSailio").html(asiakirja);
			});
			$.get("kommentit.php", {id: asiakirja_id}, function(kommentit){
				$("#kommenttisailio").html(kommentit);
			});
			$("#kommentit").show();
		}
		else {
			$.get("asiakirja.php", function(asiakirja){
				$("#artikkeliSailio").html(asiakirja);
			});
			
			$("#kommentit").hide();
		}
		
	}
});