<?php
// T�m� funktioesimerkki on kopioitu kirjasta PHP ja MySQL: Tietokantapohjaiset verkkopalvelut (Heinisuo ja Rauta, 2007)
// Esimerkki� saa k�ytt�� n�yt�ss� sellaisenaan - ei kannata keksi� py�r�� uudelleen

// tietokantaan yhdist�minen
// palauttaa yhteysresurssin
function yhdista_tietokantaan()
{
  // yritet��n yhdist��
  $yhteys = mysql_connect("localhost", "root", "");

  // yhteytt� ei saatu, suoritus lopetetaan
  if($yhteys == false)
    exit("Tietokantapalvelimeen yhdist�minen ep�onnistui.");

  // valitaan kanta ja tarkistetaan onnistuiko
  // vaihda t�h�n oman tietokantasi nimi
  if(!mysql_select_db("kommentointisivusto", $yhteys))
    exit("Tietokannan valinta ep�onnistui.");
	mysql_set_charset("utf8", $yhteys);
  return $yhteys;
}

?>