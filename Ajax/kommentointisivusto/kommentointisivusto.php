<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Asiakirjat</title>
	<link rel="stylesheet" type="text/css" href="kommentointisivusto.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
	<script src="kommentointisivusto.js"></script>
</head>

<body>

<div id="sailio">
	<header>
		<h1>Asiakirjat - odotamme kommenttejasi</h1>
	</header>
	<nav>
		<select id="asiakirjavalinta">
			<option value="kaikki" selected>Kaikki asiakirjat</option>
			<?php
				header("Content-Type: text/html; charset=utf-8");
				require("funktiot.php");
				$yhteys = yhdista_tietokantaan();
				
				$sql = "select * from asiakirja";
				$tulos = mysql_query($sql, $yhteys);
				while($asiakirjat = mysql_fetch_assoc($tulos)) {
					echo "<option value=\"" . $asiakirjat["asiakirja_id"] . "\">" . $asiakirjat["otsikko"] . "</option>";
				}
				
			?>
		</select>
	</nav>
	<main>
		<div id="artikkeliSailio">
		</div>
		<section id="kommentit">
			
			<div id="kommenttisailio">
			</div>
			<div id="lisaa-kommentti">
			<div id="virheilmoitus"></div>
				<h2>Lisää oma kommenttisi</h2>
				<p><span class="kommentti-label">Nimi tai nimimerkki:</span> <input type="text" id="kommenttinimimerkki" size="30"></p>
				<p><span class="kommentti-label">Kommenttiotsikko:</span> <input type="text" id="kommenttiotsikko" size="30"></p>
				<p><textarea id="kommenttiteksti" cols="60" rows="10"></textarea></p>
				<p><button id="laheta-kommentti">Lähetä</button>
			</div>
		</section>
	</main>
</div>
</body>
</html>