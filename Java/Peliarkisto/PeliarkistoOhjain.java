
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class PeliarkistoOhjain {
    private PeliarkistoGUI GUI;
    private PeliarkistoMalli malli;
    
    public PeliarkistoOhjain(PeliarkistoGUI GUI, PeliarkistoMalli malli) {
        this.GUI = GUI;
        this.malli = malli;
    }
    public void lisaaPeli() {
        String nimi = GUI.annaLisattavaNimi();
        String jarjestelma = GUI.annaLisattavaJarjestelma();
        String kommentti = GUI.annaLisattavaKommentti();
        Tila tila = GUI.annaLisattavaTila();
        Arviointi arviointi = GUI.annaLisattavaArviointi();
        boolean pelaan = GUI.annaLisattavaPelaanNyt();
        malli.lisaaPeli(nimi, jarjestelma, kommentti, tila, arviointi, pelaan);

    }
    public void tyhjennaKentat() {
        GUI.asetaNimi("");
        GUI.asetaJarjestelma("");
        GUI.asetaTila(null);
        GUI.asetaPelaanNyt(false);
        GUI.asetaArviointi(null);
        GUI.asetaKommentti("");
    }
    public void poistaPeli(int index) {
        malli.poistaPeli(index);
    }
    public void muokaaPelia(int index) {
        String nimi = GUI.annaLisattavaNimi();
        String jarjestelma = GUI.annaLisattavaJarjestelma();
        String kommentti = GUI.annaLisattavaKommentti();
        Tila tila = GUI.annaLisattavaTila();
        Arviointi arviointi = GUI.annaLisattavaArviointi();
        boolean pelaan = GUI.annaLisattavaPelaanNyt();
        malli.muokaaPelia(index, nimi, jarjestelma, kommentti, tila, arviointi, pelaan);

    }
    public void naytaPeli(int index) {
        String nimi = malli.annaPelinNimi(index);
        String jarjestelma = malli.annaPelinJarjestelma(index);
        String kommentti = malli.annaPelinKommentti(index);
        Tila tila = malli.annaPelinTila(index);
        Arviointi arviointi = malli.annaPelinArviointi(index);
        boolean pelaan = malli.annaPelaanNyt(index);
        GUI.asetaNimi(nimi);
        GUI.asetaJarjestelma(jarjestelma);
        GUI.asetaTila(tila);
        GUI.asetaPelaanNyt(pelaan);
        GUI.asetaArviointi(arviointi);
        GUI.asetaKommentti(kommentti);
        
    }
    public void tallennaLevylle() {
        try {
            FileOutputStream tKirjoittaja = new FileOutputStream("PeliarkistoMalli.dat");
            ObjectOutputStream oliKirjoittaja = new ObjectOutputStream(tKirjoittaja);
            oliKirjoittaja.writeObject(malli);
            oliKirjoittaja.close();
        }
        catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }
    public void lataaLevylta() {
        try {
            FileInputStream tLukija = new FileInputStream("PeliarkistoMalli.dat");
            ObjectInputStream olioLukija = new ObjectInputStream(tLukija);
            try {
                malli = (PeliarkistoMalli)olioLukija.readObject();
            }
            catch(ClassNotFoundException e) {}
            
            olioLukija.close();
            String[] peliNimet = new String[malli.annaArkistoMaara()];
            for(int i = 0; i<peliNimet.length;i++) {
                peliNimet[i] = malli.annaPelinNimi(i);
            }
            GUI.lataaPeliLuettelo(peliNimet);
        }
        catch(IOException ioe){
            System.out.println(ioe.getMessage());
        }
        }
    }
