import java.util.ArrayList;
import java.io.*;

public class PeliarkistoMalli implements Serializable{
    private ArrayList<Peliarkisto> arkisto;
    
    public PeliarkistoMalli() {
        this.arkisto = new ArrayList<>();
    }
    public int annaArkistoMaara() {
        return arkisto.size();
    }
    public String annaPelinNimi(int nro) {
        return arkisto.get(nro).annaNimi();
    }
    public String annaPelinJarjestelma(int nro) {
        return arkisto.get(nro).annaJarjestelma();
    }
    public String annaPelinKommentti(int nro) {
        return arkisto.get(nro).annaKommentti();
    }
    public Tila annaPelinTila(int nro) {
        return arkisto.get(nro).annaTila();
    }
    public boolean annaPelaanNyt(int nro) {
        return arkisto.get(nro).annaPelaan();
    }
    public Arviointi annaPelinArviointi(int nro) {
        return arkisto.get(nro).annaArviointi();
    }
    public void lisaaPeli(String nimi, String jarjestelma, String kommentti, Tila tila, Arviointi arviointi, boolean pelaan) {
        Peliarkisto a = new Peliarkisto(nimi, jarjestelma, kommentti, tila, arviointi, pelaan);
        arkisto.add(a);
    }
    public void poistaPeli(int nro) {
        arkisto.remove(nro);
    }
    public void muokaaPelia(int nro, String nimi, String jarjestelma, String kommentti, Tila tila, Arviointi arviointi, boolean pelaan) {
        Peliarkisto a = new Peliarkisto(nimi, jarjestelma, kommentti, tila, arviointi, pelaan);
        arkisto.set(nro, a);
    }
}
