import java.io.*;
public class Peliarkisto implements Serializable {
    private String nimi;
    private String jarjestelma;
    private String kommentti;
    private Tila tila;
    private Arviointi arviointi;
    private boolean pelaan;
    
    public Peliarkisto(String nimi, String jarjestelma, String kommentti, Tila tila, Arviointi arviointi, boolean pelaan) {
        this.nimi = nimi;
        this.kommentti = kommentti;
        this.jarjestelma = jarjestelma;
        this.tila = tila;
        this.arviointi = arviointi;
        this.pelaan = pelaan;
    }
    public String annaNimi() {
        return nimi;
    }
    public String annaJarjestelma() {
        return jarjestelma;
    }
    public String annaKommentti() {
        return kommentti;
    }
    public Tila annaTila() {
        return tila;
    }
    public Arviointi annaArviointi() {
        return arviointi;
    }
    public boolean annaPelaan() {
        return pelaan;
    }
    
}
