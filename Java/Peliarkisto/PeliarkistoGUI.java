import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.border.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class PeliarkistoGUI implements Runnable {
	private JFrame frame;
        private JMenuItem menu;
        private JMenuItem menu2;
        private JList<String> luettelo;
        private DefaultListModel<String> lista;
        private JTextField kentta;
        private JTextField kentta2;
        private JRadioButton vaihtoehto;
        private JRadioButton vaihtoehto2;
        private JRadioButton vaihtoehto3;
        private JRadioButton vaihtoehto4;
        private JCheckBox ruutu;
        private JRadioButton nappi;
        private JRadioButton nappi2;
        private JRadioButton nappi3;
        private JRadioButton nappi4;
        private JRadioButton nappi5;
        private JRadioButton nappi6;
        private JTextArea kommentti;
        private JButton nappi7;
        private JButton nappi8;
        private JButton nappi9;
        private JButton nappi10;
        private PeliarkistoOhjain ohjain;
        
        public PeliarkistoGUI(PeliarkistoMalli malli) {
        this.ohjain = new PeliarkistoOhjain(this, malli);
        }
        
	public void run() {

	frame = new JFrame("Peliarkisto");
        
        luoKomponentit(frame.getContentPane());

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);      

        frame.pack();
        frame.setVisible(true);
    }
    
    private void luoKomponentit(Container sailio) {
    	JMenuBar menuBar = new JMenuBar();
    	JMenu Menu = new JMenu("Tiedosto");
    	menuBar.add(Menu);
    	menu = new JMenuItem("Tallena arkisto");
    	menu2 = new JMenuItem("Lataa arkisto");
    	JMenuItem menu3 = new JMenuItem("Sulje");
    	Menu.add(menu);
    	Menu.add(menu2);
    	Menu.add(menu3);
    	frame.setJMenuBar(menuBar);
        
    	lista = new DefaultListModel<String>();
    	luettelo = new JList<String>(lista);
    	JScrollPane panel = new JScrollPane(luettelo);
    	panel.setPreferredSize(new Dimension(200, 200));
    	sailio.add(panel);
    	
    	JPanel panel2 = new JPanel();
    	BoxLayout asemointi = new BoxLayout(panel2, BoxLayout.PAGE_AXIS);
    	panel2.setLayout(asemointi);
    	JPanel tiedot = new JPanel();
    	TitledBorder title;
    	title = BorderFactory.createTitledBorder("Perustiedot");
    	tiedot.setBorder(title);
    	JPanel panel3 = new JPanel();
    	GridLayout asem = new GridLayout(2,1);
    	panel3.setLayout(asem);
    	JPanel panel4 = new JPanel();
    	panel4.setLayout(asem);
    	JLabel nimi = new JLabel("Nimi");
    	kentta = new JTextField(30);
    	JLabel jarjestelma = new JLabel("Järjestemä");
    	kentta2 = new JTextField(30); 
    	panel3.add(nimi);
    	panel4.add(kentta);
    	panel3.add(jarjestelma);
    	panel4.add(kentta2);
    	tiedot.add(panel3);
    	tiedot.add(panel4);
    	panel2.add(tiedot);
    	
    	JPanel paneli = new JPanel();
    	BoxLayout asemointi2 = new BoxLayout(paneli, BoxLayout.PAGE_AXIS);
    	paneli.setLayout(asemointi2);
    	TitledBorder title2;
    	title2 = BorderFactory.createTitledBorder("Tila");
    	paneli.setBorder(title2);
    	JPanel tila = new JPanel(new FlowLayout(FlowLayout.LEADING));
    	ButtonGroup ryhma = new ButtonGroup();
    	vaihtoehto = new JRadioButton("aloittamatta");
    	vaihtoehto2 = new JRadioButton("kesken");
    	vaihtoehto3 = new JRadioButton("pelattu läpi");
    	vaihtoehto4 = new JRadioButton("extrat tehty");
    	tila.add(vaihtoehto);
    	tila.add(vaihtoehto2);
    	tila.add(vaihtoehto3);
    	tila.add(vaihtoehto4);
    	paneli.add(tila);
    	JPanel ruutua = new JPanel(new FlowLayout(FlowLayout.LEADING));
    	ruutu = new JCheckBox("Pelaan nyt");
    	ruutua.add(ruutu);
    	paneli.add(ruutua);
    	panel2.add(paneli);
    	
    	JPanel paneli2 = new JPanel();
    	BoxLayout asemointi3 = new BoxLayout(paneli2, BoxLayout.PAGE_AXIS);
    	paneli2.setLayout(asemointi3);
    	TitledBorder title3;
    	title3 = BorderFactory.createTitledBorder("Arviointi");
    	paneli2.setBorder(title3);
    	JPanel arvioi = new JPanel(new FlowLayout(FlowLayout.LEADING));
    	JLabel teksti = new JLabel("Arvosana: ");
    	ButtonGroup ryhma2 = new ButtonGroup();
    	nappi = new JRadioButton(" 1");
    	nappi2 = new JRadioButton(" 2");
    	nappi3 = new JRadioButton(" 3");
    	nappi4 = new JRadioButton(" 4");
    	nappi5 = new JRadioButton(" 5");
    	nappi6 = new JRadioButton(" Ei arviointia");
    	arvioi.add(teksti);
    	arvioi.add(nappi);
    	arvioi.add(nappi2);
    	arvioi.add(nappi3);
    	arvioi.add(nappi4);
    	arvioi.add(nappi5);
    	arvioi.add(nappi6);
    	paneli2.add(arvioi);
    	panel2.add(paneli2);
    	JPanel kommentoi = new JPanel(new FlowLayout(FlowLayout.LEADING));
    	JLabel kommentit = new JLabel("Kommentit:");
    	kommentti = new JTextArea(5, 20);
    	JScrollPane scrollPane = new JScrollPane(kommentti);
    	kommentoi.add(kommentit);
    	paneli2.add(kommentoi);
    	paneli2.add(kommentti);
        
        JPanel panel5 = new JPanel();
        nappi7 = new JButton("Tallena muutokset");
        nappi7.setEnabled(false);
        nappi8 = new JButton("Lisää uutena");
        nappi8.setEnabled(false);
        nappi9 = new JButton("Poista peli");
        nappi9.setEnabled(false);
        nappi10 = new JButton("Tyhjennä kentät");
        panel5.add(nappi7);
        panel5.add(nappi8);
        panel5.add(nappi9);
        panel5.add(nappi10);
        panel2.add(panel5);
        
    	JLabel teksti2 = new JLabel("Peliarkisto (C) Verneri Peltola", SwingConstants.CENTER);
    	sailio.add(teksti2, BorderLayout.PAGE_END);
    	
    	JSplitPane jakoPaneeli = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel, panel2);
    	sailio.add(jakoPaneeli);
        
        menu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ohjain.tallennaLevylle();
            }
        });
        menu2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ohjain.lataaLevylta();
            }
        });
        menu3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
            }
        });
        
        nappi7.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ohjain.muokaaPelia(luettelo.getSelectedIndex());
                lista.setElementAt(annaLisattavaNimi(), luettelo.getSelectedIndex());
            }
        });
        nappi8.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ohjain.lisaaPeli();
                lista.addElement(annaLisattavaNimi());
            }
        });
        nappi10.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ohjain.tyhjennaKentat();
                luettelo.clearSelection();
            }
        });
        nappi9.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ohjain.poistaPeli(luettelo.getSelectedIndex());
                lista.remove(luettelo.getSelectedIndex());
            }
        });
        
        luettelo.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if(luettelo.getSelectedIndex() != -1) {
                    ohjain.naytaPeli(luettelo.getSelectedIndex());
                    vapautaNappi9();
                }
                else {
                    ohjain.tyhjennaKentat();
                    lukitseNappi7();
                    lukitseNappi9();
                }
            }
        });
        DocumentListener documentListener = new DocumentListener() {
            public void changedUpdate(DocumentEvent documentEvent) {
                
            }
            public void insertUpdate(DocumentEvent documentEvent) {
                if(kentta.getText().length() == 0) {
                    lukitseNappi7();
                    lukitseNappi8();
                    
                }
                else {
                    if(luettelo.getSelectedIndex() != -1) {
                        vapautaNappi7();
                    }
                    vapautaNappi8();
                }
            }
            public void removeUpdate(DocumentEvent documentEvent) {
                if(kentta.getText().length() == 0) {
                    lukitseNappi7();
                    lukitseNappi8();
                    
                }
                else {
                    vapautaNappi7();
                    vapautaNappi8();
                }
            }
        };
        kentta.getDocument().addDocumentListener(documentListener);
    }

    public void lukitseNappi7() {
        nappi7.setEnabled(false);
    }
    public void lukitseNappi8() {
        nappi8.setEnabled(false);
    }
    public void lukitseNappi9() {
        nappi9.setEnabled(false);
    }
    public void lukitseNappi10() {
        nappi10.setEnabled(false);
    }
    public void vapautaNappi7() {
        nappi7.setEnabled(true);
    }
    public void vapautaNappi8() {
        nappi8.setEnabled(true);
    }
    public void vapautaNappi9() {
        nappi9.setEnabled(true);
    }
    public void vapautaNappi10() {
        nappi10.setEnabled(true);
    }
    public String annaLisattavaNimi() {
        return kentta.getText();
    }
    public String annaLisattavaJarjestelma() {
        return kentta2.getText();
    }
    public String annaLisattavaKommentti() {
        return kommentti.getText();
    }
    public Tila annaLisattavaTila() {
        if(vaihtoehto.isSelected()) {
            return Tila.ALOITTAMATTA;
        }
        else if(vaihtoehto2.isSelected()) {
            return Tila.KESKEN;
        }
        else if(vaihtoehto3.isSelected()) {
            return Tila.PELATTU_LAPI;
        }
        else if(vaihtoehto.isSelected()) {
            return Tila.EXTRAT_TEHTY;
        }
        return Tila.ALOITTAMATTA;
    }
    public Arviointi annaLisattavaArviointi() {
        if(nappi.isSelected()) {
            return Arviointi.YKSI;
        }
        else if(nappi2.isSelected()) {
            return Arviointi.KAKSI;
        }
        else if(nappi3.isSelected()) {
            return Arviointi.KOLME;
        }
        else if(nappi4.isSelected()) {
            return Arviointi.NELJA;
        }
        else if(nappi5.isSelected()) {
            return Arviointi.VIISI;
        }
        else if(nappi6.isSelected()) {
            return Arviointi.EI_ARVOINTIA;
        }
        return Arviointi.YKSI;
        
    }
    public boolean annaLisattavaPelaanNyt() {
        if(ruutu.isSelected()) {
            return true;
        }
        else {
            return false;
        }
    }
    public void asetaNimi(String teksti) {
        kentta.setText(teksti);
    }
    public void asetaJarjestelma(String teksti) {
        kentta2.setText(teksti);
    }
    public void asetaTila(Tila tila) {
        if(tila == Tila.ALOITTAMATTA) {
            vaihtoehto.setSelected(true);
        }
        else if(tila == Tila.KESKEN) {
            vaihtoehto2.setSelected(true);
        }
        else if(tila == Tila.PELATTU_LAPI) {
            vaihtoehto3.setSelected(true);
        }
        else if(tila == Tila.EXTRAT_TEHTY){
            vaihtoehto4.setSelected(true);
        }
        else {
            vaihtoehto.setSelected(false);
            vaihtoehto2.setSelected(false);
            vaihtoehto3.setSelected(false);
            vaihtoehto4.setSelected(false);
        }
    }
    public void asetaPelaanNyt(boolean pelaan) {
        if(pelaan) {
            ruutu.setSelected(true);
        }
        else {
            ruutu.setSelected(false);
        }
    }
    public void asetaArviointi(Arviointi arviointi) {
        if(arviointi == Arviointi.YKSI) {
            nappi.setSelected(true);
        }
        else if(arviointi == Arviointi.KAKSI) {
            nappi2.setSelected(true);
        }
        else if(arviointi == Arviointi.KOLME) {
            nappi3.setSelected(true);
        }
        else if(arviointi == Arviointi.NELJA) {
            nappi4.setSelected(true);
        }
        else if(arviointi == Arviointi.YKSI) {
            nappi5.setSelected(true);
        }
        else if(arviointi == Arviointi.EI_ARVOINTIA) {
            nappi6.setSelected(true);
        }
        else {
            nappi.setSelected(false);
            nappi2.setSelected(false);
            nappi3.setSelected(false);
            nappi4.setSelected(false);
            nappi5.setSelected(false);
            nappi6.setSelected(false);
        }
    }
    public void asetaKommentti(String teksti) {
        kommentti.setText(teksti);
    }
    public void lataaPeliLuettelo(String[] lataa) {
        lista.removeAllElements();
        for(int i = 0; i<lataa.length;i++) {
            lista.addElement(lataa[i]);
        }
    }
    public static void main(String[] args) {
        PeliarkistoMalli malli = new PeliarkistoMalli();
   	PeliarkistoGUI testi = new PeliarkistoGUI(malli);
   	SwingUtilities.invokeLater(testi);
    }
}