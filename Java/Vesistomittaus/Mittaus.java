public class Mittaus {
    private String aika;
    private String paikka;
    private String tulos;
    
    public Mittaus(String aika, String paikka, String tulos) {
        this.aika = aika;
        this.paikka = paikka;
        this.tulos = tulos;
    }
    public String annaAika() {
       return aika; 
    }
    public String annaPaikka() {
        return paikka;
    }
    public String annaTulos() {
        return tulos;
    }
}
