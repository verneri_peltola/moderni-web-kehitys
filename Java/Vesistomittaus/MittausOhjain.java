import java.time.LocalDateTime;
public class MittausOhjain {
    private MittausGUI GUI;
    private MittausMalli malli;
    private int mittaus_nro;
    
    public MittausOhjain(MittausGUI GUI, MittausMalli malli) {
        this.GUI = GUI;
        this.malli = malli;
        mittaus_nro = 0;
    }
    
    public void paivitaAika() {
        LocalDateTime aika = LocalDateTime.now();
        int vuosi = aika.getYear();
        int kuukausi = aika.getMonthValue();
        int paiva = aika.getDayOfMonth();
        int tunti = aika.getHour();
        int minuutti = aika.getMinute();
        int sekuntti = aika.getSecond();
        String tuntiMerkki;
        String minuuttiMerkki;
        String sekunttiMerkki;
        if (tunti < 10) {
            tuntiMerkki = "0"+tunti;
        }
        else {
            tuntiMerkki = ""+tunti;
        }
        if (minuutti < 10) {
            minuuttiMerkki = "0"+minuutti;
        }
        else {
            minuuttiMerkki = ""+minuutti;
        }
        if (sekuntti < 10) {
            sekunttiMerkki = "0"+sekuntti;
        }
        else {
            sekunttiMerkki = ""+sekuntti;
        }
        
        String pvm = paiva + "." + kuukausi + "." + vuosi;
        String klo = tuntiMerkki + ":" + minuuttiMerkki + ":" + sekunttiMerkki;
        GUI.paivitaAika(pvm, klo);
    }
    public void lisaaMittaus() {
        String kello = GUI.annaLisattavaKello();
        String pvm = GUI.annaLisattavaPvm();
        String paikka = GUI.annaLisattavaPaikka();
        String tulos = GUI.annaLisattavaTulos();
        malli.lisaaMittaus(pvm, kello, paikka, tulos);
        naytaUusinMittaus();
        GUI.nollaaLisays();
    }
    public void naytaUusinMittaus() {
        int indeksi = malli.annaMittausMaara()-1;
        if(indeksi < 0) {
            GUI.asetaNaytettavaAika("");
            GUI.asetaNaytettavaPaikka("");
            GUI.asetaNaytettavaTulos("");
            GUI.lukitseNappi();
            GUI.lukitseNappi2();
            GUI.lukitseNappi3();
        }
        else {
            String naytettavaAika = malli.annaMittauksenAika(indeksi);
            String naytettavaPaikka = malli.annaMittauksenPaikka(indeksi);
            String naytettavaTulos = malli.annaMittauskenTulos(indeksi);
            GUI.asetaNaytettavaAika(naytettavaAika);
            GUI.asetaNaytettavaPaikka(naytettavaPaikka);
            GUI.asetaNaytettavaTulos(naytettavaTulos);
            if (indeksi == 0) {
                GUI.lukitseNappi();
            }
            else {
                GUI.vapautaNappi();
            }
            GUI.lukitseNappi3();
            GUI.lukitseNappi2();
            mittaus_nro = indeksi;
        }
        
    }
    public void naytaSeuraavaMittaus() {
        mittaus_nro++;
        String naytettavaAika = malli.annaMittauksenAika(mittaus_nro);
        String naytettavaPaikka = malli.annaMittauksenPaikka(mittaus_nro);
        String naytettavaTulos = malli.annaMittauskenTulos(mittaus_nro);
        GUI.asetaNaytettavaAika(naytettavaAika);
        GUI.asetaNaytettavaPaikka(naytettavaPaikka);
        GUI.asetaNaytettavaTulos(naytettavaTulos);
        if (mittaus_nro > 0) {
            GUI.vapautaNappi();
        }
        else {
            GUI.lukitseNappi();
        }
        if (mittaus_nro == malli.annaMittausMaara()-1) {
            GUI.lukitseNappi2();
            GUI.lukitseNappi3();
        }
        else {
            GUI.vapautaNappi2();
            GUI.vapautaNappi3();
        }
        
    }
    public void naytaEdellinenMittaus() {
        mittaus_nro--;
        String naytettavaAika = malli.annaMittauksenAika(mittaus_nro);
        String naytettavaPaikka = malli.annaMittauksenPaikka(mittaus_nro);
        String naytettavaTulos = malli.annaMittauskenTulos(mittaus_nro);
        GUI.asetaNaytettavaAika(naytettavaAika);
        GUI.asetaNaytettavaPaikka(naytettavaPaikka);
        GUI.asetaNaytettavaTulos(naytettavaTulos);
        if (mittaus_nro > 0) {
            GUI.vapautaNappi();
        }
        else {
            GUI.lukitseNappi();
        }
        if (mittaus_nro == malli.annaMittausMaara()-1) {
            GUI.lukitseNappi2();
            GUI.lukitseNappi3();
        }
        else {
            GUI.vapautaNappi2();
            GUI.vapautaNappi3();
        }
    }
}
