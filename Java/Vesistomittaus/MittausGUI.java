import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;

public class MittausGUI implements Runnable {

    private JFrame frame;
    private JTextField AikaKentta;
    private JTextField PaikkaKentta;
    private JTextArea TextArea;
    private JButton nappi;
    private JButton nappi2;
    private JButton nappi3;
    private JTextField PvmKentta;
    private JTextField AikaKentta2;
    private JTextField PaikkaKentta2;
    private JTextArea TextArea2;
    private JButton nappi4;
    private MittausOhjain ohjain;
    
    public MittausGUI(MittausMalli malli) {
        this.ohjain = new MittausOhjain(this, malli);
    }
    
    public void run() {
        frame = new JFrame("Vesistömittaukset");
        
        luoKomponentit(frame.getContentPane());
        ohjain.naytaUusinMittaus();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); 

        frame.setFocusable(true); // mahdollistetaan fokuksen saaminen
        
        frame.pack();
        frame.setVisible(true);
    }    
    private void luoKomponentit(Container sailio) {
       JPanel paneli3 = new JPanel();
       JLabel otsikko = new JLabel("Vesistömittaus");
       paneli3.add(otsikko);
       sailio.add(paneli3, BorderLayout.PAGE_START);
       JPanel wrapper = new JPanel();
       wrapper.setBorder(new EmptyBorder(10,10,10,10));
       JPanel panel = new JPanel();
       Border blackline = BorderFactory.createLineBorder(Color.black);
       panel.setBorder(blackline);
       BoxLayout asem = new BoxLayout(panel, BoxLayout.PAGE_AXIS);
       panel.setLayout(asem);
       JPanel panel3 = new JPanel(new FlowLayout(FlowLayout.LEADING));
       JLabel teksti = new JLabel("Aika:      ");
       AikaKentta = new JTextField(15);
       AikaKentta.setEditable(false);
       JPanel panel4 = new JPanel(new FlowLayout(FlowLayout.LEADING));
       JLabel teksti2 = new JLabel("Paikka: ");
       PaikkaKentta = new JTextField(15);
       PaikkaKentta.setEditable(false);
       JPanel panel8 = new JPanel(new FlowLayout(FlowLayout.LEADING));
       JLabel teksti3 = new JLabel("Mittaustulos:");
       TextArea = new JTextArea(5, 20);
       JPanel paneli = new JPanel(new FlowLayout(FlowLayout.LEADING));
       nappi = new JButton("<< Edellinen");
       nappi2 = new JButton("Uusin mittaus");
       nappi3 = new JButton("Seuraava >>");
       panel3.add(teksti);
       panel3.add(AikaKentta);
       panel.add(panel3);
       panel4.add(teksti2);
       panel4.add(PaikkaKentta);
       panel.add(panel4);
       panel8.add(teksti3);
       panel.add(panel8);
       panel.add(TextArea);
       paneli.add(nappi);
       paneli.add(nappi2);
       paneli.add(nappi3);
       panel.add(paneli);
       wrapper.add(panel);
       sailio.add(wrapper, BorderLayout.LINE_START);
       
       JPanel wrapper2 = new JPanel();
       wrapper2.setBorder(new EmptyBorder(10,10,10,10));
       JPanel panel2 = new JPanel();
       Border blackline2 = BorderFactory.createLineBorder(Color.black);
       panel2.setBorder(blackline2);
       BoxLayout asem2 = new BoxLayout(panel2, BoxLayout.PAGE_AXIS);
       panel2.setLayout(asem2);
       JPanel panel5 = new JPanel(new FlowLayout(FlowLayout.LEADING));
       JLabel teksti4 = new JLabel("Pvm:      ");
       PvmKentta = new JTextField(10);
       PvmKentta.setEditable(false);
       JLabel teksti5 = new JLabel("Klo:   ");
       AikaKentta2 = new JTextField(5);
       AikaKentta2.setEditable(false);
       JPanel panel6 = new JPanel(new FlowLayout(FlowLayout.LEADING));
       JLabel teksti6 = new JLabel("Paikka: ");
       PaikkaKentta2 = new JTextField(20);
       JPanel panel7 = new JPanel(new FlowLayout(FlowLayout.LEADING));
       JLabel teksti7 = new JLabel("Mittaustulos:");
       TextArea2 = new JTextArea(5, 20);
       JPanel paneli2 = new JPanel(new FlowLayout(FlowLayout.TRAILING));
       nappi4 = new JButton("Lisää mittaus");
       panel5.add(teksti4);
       panel5.add(PvmKentta);
       panel5.add(teksti5);
       panel5.add(AikaKentta2);
       panel2.add(panel5);
       panel6.add(teksti6);
       panel6.add(PaikkaKentta2);
       panel2.add(panel6);
       panel7.add(teksti7);
       panel2.add(panel7);
       panel2.add(TextArea2);
       paneli2.add(nappi4);
       panel2.add(paneli2);
       wrapper2.add(panel2);
       sailio.add(wrapper2, BorderLayout.LINE_END);
       
        ActionListener ajastinKuuntelija = new ActionListener(){
            public void actionPerformed(ActionEvent event){
                ohjain.paivitaAika();
            }
        };
        Timer ajastin = new Timer(1000, ajastinKuuntelija);
        ajastin.setInitialDelay(0);
        ajastin.start();
       
        nappi.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ohjain.naytaEdellinenMittaus();
            }
        });
        nappi2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ohjain.naytaUusinMittaus();
            }
        });
        nappi3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ohjain.naytaSeuraavaMittaus();
            }
        });
        nappi4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ohjain.lisaaMittaus();
            }
        });
        
    }
    public void asetaNaytettavaAika(String teksti) {
        AikaKentta.setText(teksti);
    }
    public void asetaNaytettavaPaikka(String teksti) {
        PaikkaKentta.setText(teksti);
    }
    public void asetaNaytettavaTulos(String teksti) {
        TextArea.setText(teksti);
    }
    public void lukitseNappi() {
        nappi.setEnabled(false);
    }
    public void lukitseNappi2() {
        nappi2.setEnabled(false);
    }
    public void lukitseNappi3() {
        nappi3.setEnabled(false);
    }
    public void vapautaNappi() {
        nappi.setEnabled(true);
    }
    public void vapautaNappi2() {
        nappi2.setEnabled(true);
    }
    public void vapautaNappi3() {
        nappi3.setEnabled(true);
    }
    public String annaLisattavaKello() {
        return AikaKentta2.getText();
    }
    public String annaLisattavaPvm() {
        return PvmKentta.getText();
    }
    public String annaLisattavaPaikka() {
        return PaikkaKentta2.getText();
    }
    public String annaLisattavaTulos() {
        return TextArea2.getText();
    }
    public void nollaaLisays() {
        PaikkaKentta2.setText("");
        TextArea2.setText("");
    }
    public void paivitaAika(String pvm, String klo) {
        PvmKentta.setText(pvm);
        AikaKentta2.setText(klo);
    }
    
    public static void main(String[] args) {
        MittausMalli malli = new MittausMalli();
        MittausGUI testi = new MittausGUI(malli);
        SwingUtilities.invokeLater(testi);
    }
}