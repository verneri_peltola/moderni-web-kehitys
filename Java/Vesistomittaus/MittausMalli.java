import java.util.ArrayList;

public class MittausMalli {
    private ArrayList<Mittaus> mittaus;
    
    public MittausMalli() {
        mittaus = new ArrayList<Mittaus>();
    }
    public int annaMittausMaara() {
        return mittaus.size();
    }
    public String annaMittauksenAika(int nro) {
        return mittaus.get(nro).annaAika();
    }
    public String annaMittauksenPaikka(int nro) {
        return mittaus.get(nro).annaPaikka();
    }
    public String annaMittauskenTulos(int nro) {
        return mittaus.get(nro).annaTulos();
    }
    public void lisaaMittaus(String pvm, String klo, String paikka, String tulos) {
        String Aika = pvm + "  Klo " + klo;
        Mittaus m = new Mittaus(Aika, paikka, tulos);
        mittaus.add(m);
    }
}
